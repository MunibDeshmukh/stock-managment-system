<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function CustomerShowForm(){

        return view('admin.customer.customer_add');
    }

    public function CustomerAdd(Request $request){

       // dd($request->input());
        $arr=array();
        $customer=new Customer();
        $arr=$request->input();
        $customer->fill($arr);
        $customer->save();
        return redirect(route('CustomerShow'));

    }

    public function CustomerUpdateForm(Request $request){

        $customer=Customer::find($request->id);
        return view('admin.customer.customer_update')->with(['customer'=>$customer]);
    }

    public function CustomerUpdate(Request $request){

       // dd($request->input());
        $customer=Customer::find($request->id);
        $arr=array();
        $arr=$request->input();
        $customer->fill($arr);
        $customer->update();
        // dd($customer);
        return redirect(route('CustomerShow'));

    }

    public function CustomerShow(){

        $customers=Customer::all();
        //dd($customers);
        return view('admin.customer.customers')->with(['customers'=>$customers]);

    }

    public function CustomerRemove(){
        $customer=Customer::find($request->id);
        $customer->delete();
        return redirect(route('CustomerShow'));

    }
}
