<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //
    public function Login(){
        //dd(session('user'));
        return view("admin.login");
    }

    public function SignIn(Request $request){
        $validatedData = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

            $name=$request->username;
            $password=$request->password;

            $user=User::where('username',$name)->first();
            //dd(Hash::check($password, $user->password));

           //dd($password);
            if(empty($user)){
                return redirect(route('LoginShow'))->with('message','Username or password is invalid');
            }
            else{
               // $hashpass=Hash::make($password);
                if(Hash::check($password, $user->password)){

                    $request->session()->put('user',$user);
                    return redirect(route('Home'));;

                }
                else {
                    return redirect(route('LoginShow'))->with('message','Username or password is invalid');
                }
            }

    }

    public function CreateUser(Request $request){

        $validatedData = $request->validate([
                    'username' => 'required',
                    'email' => 'required',
                    'password' => 'required',
                ]);



                $user =User::where('username',$request->username)->first();;

                if(!empty($user)){
                    return redirect(route('LoginShow'))->with('message','User already exist');
                }
                else{

                    $user=new User();
                    $user->username=$request->username;
                    $user->email=$request->email;
                    $user->firstname=$request->firstname;
                    $user->lastname=$request->lastname;
                    $user->role_id=1;
                    $user->password=Hash::make($request->password);

                    $user->save();

                    return redirect(route('LoginShow'))->with('message','User created successfully');

                }

    }
}
