<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packet_size;

class PacketsizeController extends Controller
{
    public function Packet_sizeShowForm(){

        $packet_sizes=Packet_size::all();
        return view('admin.packet-size.packet-size-add')->with(['packet_sizes'=>$packet_sizes]);
    }

    public function Packet_sizeAdd(Request $request){

       // dd($request->input());
        $arr=array();
        $packet_size=new Packet_size();
        $arr=$request->input();

        $packet_size->fill($arr);
        $packet_size->save();
        return redirect(route('Packet_sizeShow'));

    }

    public function Packet_sizeUpdateForm(Request $request){

        $packet_size=Packet_size::find($request->id);
        return view('admin.packet-size.packet-size-update')->with(['packet_size'=>$packet_size]);
    }

    public function Packet_sizeUpdate(Request $request){

       // dd($request->input());
        $packet_size=Packet_size::find($request->id);
        //dd($packet_size);
        $arr=array();
        $arr=$request->input();

        $packet_size->fill($arr);
        $packet_size->update();
        return redirect(route('Packet_sizeShow'));

    }

    public function Packet_sizeShow(){

        $packet_sizes=Packet_size::all();
        return view('admin.packet-size.packet-sizes')->with(['packet_sizes'=>$packet_sizes]);

    }

    public function Packet_sizeRemove(){
        $packet_size=Packet_size::find($request->id);
        $packet_size->delete();
        return redirect(route('Packet_sizeShow'));

    }
}
