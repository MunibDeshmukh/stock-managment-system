<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Unit;
use App\Product;
use App\Stock;
use App\Packet_size;

class ProductController extends Controller
{
    public function ProductShowForm(){

        $units=Unit::all();
        return view('admin.product.product_add')->with(['units'=>$units]);
    }

    public function ProductAdd(Request $request){

       // dd($request->input());
       $pd=Product::where('prod_code',$request->prod_code)->first();
       if(!empty($pd)){
        return redirect(route('ProductShowForm'))->with('message','<div class="alert alert-danger" role="alert" style="
        margin-top: 10px;
        margin-left: 40px;
    ">
            <strong>UNSUCCESSFULL!</strong> Product with this CODE already exist.
            </div>');

       }
        $arr=array();
        $product=new Product();
        $arr=$request->input();

        $product->fill($arr);
        $product->save();
        $stock=new Stock();
        $stock->prod_id=$product->id;
        $stock->save();
        return redirect(route('ProductShow'));

    }

    public function ProductUpdateForm(Request $request){

        $product=Product::find($request->id);
        $units=Unit::all();
        return view('admin.product.product_update')->with(['product'=>$product,'units'=>$units]);
    }

    public function ProductUpdate(Request $request){

       // dd($request->input());
        $product=Product::find($request->id);
        //dd($product);
        $arr=array();
        $arr=$request->input();

      //  dd($arr);
        $product->fill($arr);
        $product->update();
        // dd($vendor);
        return redirect(route('ProductShow'));

    }

    public function ProductShow(){

        $products=Product::all();
       // dd($products[0]->Unit);
        return view('admin.product.products')->with(['products'=>$products]);

    }

    public function ProductGet(Request $request){
        $product=Product::find($request->id);

        return $product;
    }

    public function ProductRemove(){
        $vendor=Product::find($request->id);
        $vendor->delete();
        return redirect(route('ProductShow'));

    }
}
