<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Vendor;
use App\Packet_size;
use App\Lot;
use App\Purchase_order;
use App\Lot_product;
use App\Stock;

class PurchasesController extends Controller
{

    public function PurchaseShow(){

        $purchases=Purchase_order::all();
        return view('admin.purchase.purchases')->with(['purchases'=>$purchases]);
    }

    public function Purchase_ordersShowForm(){
        $vendors=Vendor::all();
        $products=Product::all();
        $packet_sizes=Packet_size::all();
        $lot_id=0;
        $lot=Lot::orderBy('id', 'DESC')->limit(1)->get();
        if(count($lot)<1){
            $lot_id=1;
        }else {
            $lot_id=$lot[0]->id;
        }
        return view('admin.purchase.purchase-order-add')->with(['vendors'=>$vendors,'products'=>$products,
                                                                'packet_sizes'=>$packet_sizes,
                                                                'lot_id'=>$lot_id]);
    }

    public function Purchase_ordersAdd(Request $request){

        $purchase_order=new Purchase_order();
      //dd($request->input());

        $arr=array();
        $arr=$request->input();
        $arr2=$request->input();
        $totalquantity=0;
        $totalprice=0;
        $i=0;
        foreach ($arr2['quantity'] as $quant) {
            $totalprice+=(int)$arr2['subtotal'][$i];
            $totalquantity+=$quant;
            $i++;
        }
        $lot=new Lot();
        $lot->quantity_inlot=$totalquantity;
       // dd($lot);
        $lot->save();
        $i=0;
        foreach ($arr2['quantity'] as $quant) {
            $arr2['lot_id']=$lot->id;
            $lot_product=new Lot_product();
            $stock=Stock::where('prod_id',$arr2['product_id'][$i])->first();
            $lot_product->lot_id=(int)$lot->id;
            $lot_product->product_id=(int)$arr2['product_id'][$i];
            $lot_product->packet_size_id=(int)$arr2['packet_size_id'][$i];
            $lot_product->quantity_inlot=(int)$arr2['quantity'][$i];
            $stock->quantity_instock+=(int)$arr2['quantity'][$i];
            $stock->quantity_purchased+=(int)$arr2['quantity'][$i];
            $lot_product->bags=(int)$arr2['bags'][$i];
            $lot_product->cost_price=(float)$arr2['cost_price'][$i];

            $lot_product->save();
            $stock->update();
            $i++;


        }

         unset($arr['packet_size_id']);
         unset($arr['cost_price']);
         unset($arr['quantity']);
         unset($arr['subtotal']);

        //array_splice($arr,'packet_size_id','1');
        $purchase_order->fill($arr);
        $purchase_order->lot_id=$lot->id;
        $purchase_order->total_amount=$totalprice;
        $purchase_order->save();
        return redirect(route('Purchase_ordersShowForm'));
    }


}
