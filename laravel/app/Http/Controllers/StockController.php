<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Lot;
use App\Lot_product;

class StockController extends Controller
{
    public function StockShow(){

        $stocks=Stock::all();
        return view('admin.stock.stock')->with(['stocks'=>$stocks]);
    }


    //------------------------------LOT------------------------------//

    public function LotShow(){
        $lots=Lot::all();
       // dd(empty($lots[1]->Childs));
        return view('admin.lot.lots')->with(['lots'=>$lots]);
    }

    public function LotMerge(Request $request){
      //  dd($request->input());
        $merge=array();
        if($request->has('do')&&$request->has('merge')){

            $t=new Lot();
            foreach ($request->merge as $lotId) {
                $lot=Lot::find($lotId);
                $lot->lot_id=$t->lot_id;
                $lot->update();
                        }

            return redirect(route('LotShow'));


        }
        else if($request->has('merge')){
            $merge=$request->merge;
            if(count($merge)>1){
               // $lot1=Lot::find($merge[0]);
                for($i=1;$i<count($merge);$i++){
                    $lot=Lot::find($merge[$i]);
                    $lot->lot_id=$merge[0];
                    $lot->update();
                }
            }
            return redirect(route('LotShow'));

        }else{
            return redirect(route('LotShow'));
        }
    }

    public function LotDetailsShow(Request $request){
        $products=array();

        $lot=Lot::where('id',$request->id)->first();
        $lotChild=$lot->Childs;
        array_push($products,$lot->id);
       foreach ($lotChild as $item) {
            array_push($products,$item->id);
        }
      //  dd($products);
        $lotproducts=Lot_product::whereIn('lot_id',$products)->get();
        return view('admin.lot.lot-detail')->with(['lotproducts'=>$lotproducts]);
       // dd($lot);

    }
}
