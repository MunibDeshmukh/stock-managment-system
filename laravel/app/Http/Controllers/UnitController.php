<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;

class UnitController extends Controller
{
    public function UnitShowForm(){

        $units=Unit::all();
        return view('admin.unit.unit_add')->with(['units'=>$units]);
    }

    public function UnitAdd(Request $request){

       // dd($request->input());
        $arr=array();
        $unit=new Unit();
        $arr=$request->input();

        $unit->fill($arr);
        $unit->save();
        return redirect(route('UnitShow'));

    }

    public function UnitUpdateForm(Request $request){

        $unit=Unit::find($request->id);
        $units=Unit::all();
        return view('admin.unit.unit_update')->with(['unit'=>$unit,'units'=>$units]);
    }

    public function UnitUpdate(Request $request){

       // dd($request->input());
        $unit=Unit::find($request->id);
        //dd($unit);
        $arr=array();
        $arr=$request->input();

        $unit->fill($arr);
        $unit->update();
        return redirect(route('UnitShow'));

    }

    public function UnitShow(){

        $units=Unit::all();
        return view('admin.unit.units')->with(['units'=>$units]);

    }

    public function UnitRemove(){
        $unit=Unit::find($request->id);
        $unit->delete();
        return redirect(route('UnitShow'));

    }
}
