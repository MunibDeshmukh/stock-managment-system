<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;

class VendorController extends Controller
{
    //

    public function VendorShowForm(){

        return view('admin.vendor.vendor_add');
    }

    public function VendorAdd(Request $request){

       // dd($request->input());
        $arr=array();
        $vendor=new Vendor();
        $arr=$request->input();
        $vendor->fill($arr);
        $vendor->save();
        return redirect(route('VendorShow'));

    }

    public function VendorUpdateForm(Request $request){

        $vendor=Vendor::find($request->id);
        return view('admin.vendor.vendor_update')->with(['vendor'=>$vendor]);
    }

    public function VendorUpdate(Request $request){

       // dd($request->input());
        $vendor=Vendor::find($request->id);
        $arr=array();
        $arr=$request->input();
        $vendor->fill($arr);
        $vendor->update();
        // dd($vendor);
        return redirect(route('VendorShow'));

    }

    public function VendorShow(){

        $vendors=Vendor::all();
        //dd($vendors);
        return view('admin.vendor.vendors')->with(['vendors'=>$vendors]);

    }

    public function VendorRemove(){
        $vendor=Vendor::find($request->id);
        $vendor->delete();
        return redirect(route('VendorShow'));

    }
}
