<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lot extends Model
{
    protected $fillable = [
        'quantity_inlot','lot_id',
    ];

    public function Lot_products(){

        return $this->hasMany('App\Lot','lot_id','id');
    }

    public function Parent()
    {
        return $this->belongsTo('App\Lot', 'lot_id');
    }

    public function Childs()
    {
        return $this->hasMany('App\Lot', 'lot_id');
    }
}
