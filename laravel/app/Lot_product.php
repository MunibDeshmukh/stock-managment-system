<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lot_product extends Model
{
    protected $fillable = [
        'lot_id','product_id','quantity_inlot','cost_price','packet_size_id','subtotal','bags',
    ];

    public function Lot(){
        return $this->belongsTo('App\Lot','lot_id');
    }

    public function Product(){
        return $this->belongsTo('App\Product','product_id');
    }

    public function Packet_size(){
        return $this->belongsTo('App\Packet_size','packet_size_id');
    }
}
