<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packet_size extends Model
{
    protected $fillable=[
        'packet_size',
    ];

    public function Lot_products(){

        return $this->hasMany('App\Lot_product','packet_size_id','id');
    }
}
