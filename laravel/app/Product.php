<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable=[
        'prod_name','prod_code','unit_id',
    ];


    public function Unit(){

       return $this->belongsTo('App\Unit','unit_id');
    }

    public function Stock(){

        return $this->hasOne('App\Stock','prod_id','id');
    }

    public function Lot_Products(){
        return $this->hasMany('App\Lot_product','product_id','id');
    }
}
