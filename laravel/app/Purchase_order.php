<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_order extends Model
{
    protected $fillable=[
        'vendor_id','total_amount','date','invoice_weight','arrival_weight','sale_weight','lot_id',
        'bl_number','gd_number',
    ];

    public function Vendor(){

        return $this->belongsTo('App\Vendor','vendor_id');
    }

    public function Lot(){

        return $this->belongsTo('App\Lot','lot_id');
    }



}
