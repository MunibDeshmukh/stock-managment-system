<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable=[
        'prod_id','quantity_instock','quantity_sold','quantity_puchased',
    ];

    public function Product(){

        return $this->belongsTo('App\Product','prod_id');
    }
}
