<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'unit',
    ];

    public function Products(){

        return $this->hasMany('App\Product','unit_id','id');
    }
}
