<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    protected $fillable = [
        'companyname', 'email', 'contact_person','contact_number','address','opening_balance',
    ];

    public function PurchaseOrders(){

        return $this->hasMany('App\Purchase_order')->orderBy('date','DESC');
    }
}
