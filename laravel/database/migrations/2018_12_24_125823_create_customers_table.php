<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->default(null);
            $table->string('contact_number')->default(null);
            $table->string('address')->default(null);
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('companyname');
            $table->string('contact_person')->default(null);
            $table->string('contact_number')->default(null);
            $table->string('email')->default(null);
            $table->float('opening_balance');
            $table->string('address')->default(null);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
