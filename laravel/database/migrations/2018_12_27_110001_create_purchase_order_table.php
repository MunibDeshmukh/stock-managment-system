<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
            $table->float('total_amount')->default(0);
            $table->date('date');
            $table->timestamps();

            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
        });

        Schema::create('pur_odr_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pur_odr_id')->unsigned();
            $table->integer('prod_id')->unsigned();
            $table->integer('quantity')->unsigned()->default(0);
            $table->float('total_amount')->default(0);
            $table->timestamps();

            $table->foreign('pur_odr_id')->references('id')->on('purchase_orders')->onDelete('cascade');
            $table->foreign('prod_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order');
    }
}
