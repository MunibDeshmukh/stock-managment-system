<html lang="en">

    <meta charset="utf-8">

    <title>Metronic | Login Page - 2</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <script src="../../../ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
      WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
    </script>

                <link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css">
                <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css">




    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico">
<style type="text/css">
    span.im-caret {
-webkit-animation: 1s blink step-end infinite;
animation: 1s blink step-end infinite;
}

@keyframes blink {
from, to {
    border-right-color: black;
}
50% {
    border-right-color: transparent;
}
}

@-webkit-keyframes blink {
from, to {
    border-right-color: black;
}
50% {
    border-right-color: transparent;
}
}

span.im-static {
color: grey;
}

div.im-colormask {
display: inline-block;
border-style: inset;
border-width: 2px;
-webkit-appearance: textfield;
-moz-appearance: textfield;
appearance: textfield;
}

div.im-colormask > input {
position: absolute;
display: inline-block;
background-color: transparent;
color: transparent;
-webkit-appearance: caret;
-moz-appearance: caret;
appearance: caret;
border-style: none;
left: 0; /*calculated*/
}

div.im-colormask > input:focus {
outline: none;
}

div.im-colormask > input::-moz-selection{
background: none;
}

div.im-colormask > input::selection{
background: none;
}
div.im-colormask > input::-moz-selection{
background: none;
}

div.im-colormask > div {
color: black;
display: inline-block;
width: 100px; /*calculated*/
}</style><style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>
<!-- end::Head -->


<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">



<div class="m-grid m-grid--hor m-grid--root m-page">


            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(assets/app/media/img/bg/bg-1.jpg);">
<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
    <div class="m-login__container">
        <div class="m-login__logo">
            <a href="#">
            <img src="assets/app/media/img/logos/logo-1.png">
            </a>
        </div>
        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">Sign In To Admin</h3>
            </div>
            <form class="m-login__form m-form" action="{{ route('signIn') }}" method="POST">
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Username" name="username" >
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                </div>
{!! csrf_field() !!}
                <div class="m-login__form-action">
                    <input type="submit"  class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary"/>
                </div>
            </form>
        </div>
        <div class="m-login__signup">
            <div class="m-login__head">
                <h3 class="m-login__title">Sign Up</h3>
                <div class="m-login__desc">Enter your details to create your account:</div>
            </div>
            <form class="m-login__form m-form" action="{{ route('createUser') }}" method="POST">
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Firstname" name="firstname">
                </div>
                <div class="form-group m-form__group">
                        <input class="form-control m-input" type="text" placeholder="Lastname" name="lastname">
                    </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Email" name="email" >
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Username" name="username" >
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="password" placeholder="Password" name="password">
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="rpassword">
                </div>
                {!! csrf_field() !!}
                <div class="m-login__form-action">
                    <input type="submit"  class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary"/>
                    &nbsp;&nbsp;
                    <button id="m_login_signup_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">Cancel</button>
                </div>
            </form>
        </div>
        <div class="m-login__forget-password">
            <div class="m-login__head">
                <h3 class="m-login__title">Forgotten Password ?</h3>
                <div class="m-login__desc">Enter your email to reset your password:</div>
            </div>
            <form class="m-login__form m-form" action="#">
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                </div>
                <div class="m-login__form-action">
                    <button id="m_login_forget_password_submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">Request</button>&nbsp;&nbsp;
                    <button id="m_login_forget_password_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">Cancel</button>
                </div>
            </form>
        </div>
        <div class="m-login__account">
            <span class="m-login__account-msg">
            Don't have an account yet ?
            </span>&nbsp;&nbsp;
            <a href="javascript:;" id="m_login_signup" class="m-link m-link--light m-login__account-link">Sign Up</a>
        </div>
    </div>
</div>
</div>


</div>
<!-- end:: Page -->


    <!--begin::Global Theme Bundle -->
                <script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
                <script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
            <!--end::Global Theme Bundle -->


                <!--begin::Page Scripts -->
                        <script src="assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>
                    <!--end::Page Scripts -->


<!-- end::Body -->

<!-- Mirrored from keenthemes.com/metronic/preview/?page=snippets/pages/user/login-2&demo=default by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Oct 2018 21:23:42 GMT -->
</body></html>
