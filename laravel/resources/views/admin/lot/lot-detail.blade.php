@extends('layouts.master')

@section('content')
<div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Lots Table
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                    {!! csrf_field() !!}
                <table id="m_table_2">
                    <thead>
                        <tr>
                                <th>Product Name</th>
                                <th>Product Code</th>
                                <th>Packet Size</th>
                                <th>Unit</th>
                                <th>Bags</th>
                                <th>Quantity</th>
                                <th>Cost Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1;?>
                        @forelse ($lotproducts as $lot)
                        <tr >
                             <td>{{ $lot->Product->prod_name }}</td>
                             <td>{{ $lot->Product->prod_code }}</td>
                             <td>{{ $lot->Packet_size->packet_size }}</td>
                             <td>{{ $lot->Product->Unit->unit }}</td>
                             <td>{{ $lot->bags }}</td>
                             <td>{{ $lot->quantity_inlot }}</td>
                             <td>{{ $lot->cost_price }}</td>
                        </tr>
                             <?php $i++;?>
                        @empty
                             <tr>
                                 <td colspan="5">Not Available</td>
                             </tr>
                        @endforelse
                    </tbody>
                </table>


            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    <script src="{{ asset('js/dataTables.min.js') }}"></script>
    <script>
            $(document).ready( function () {
                $('#m_table_2').DataTable();
            } );
    </script>

     {{--  <script src="{{ asset('/assets/demo/default/custom/crud/datatables/basic/scrollable.js') }}"></script>  --}}

@endsection
