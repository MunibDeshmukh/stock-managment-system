@extends('layouts.master')

@section('content')
<div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Lots Table
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>Add Lot</span>
                                </span>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <form action="{{ route('LotMerge') }}" method="POST">
                    {!! csrf_field() !!}
                <table id="m_table_2">
                    <thead>
                        <tr>
                                <th>Merge</th>
                                <th>Lot Id</th>
                                <th>Lot Quantity</th>
                                <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $quantity=0;$i=1;?>
                        @forelse ($lots as $lot)
                        <?php $quantity=$lot->quantity_inlot;?>
                        @if (count($lot->Childs)>0)
                        <tr role="row" class="odd">
                                <td class="m-datatable__cell m-datatable__cell--check"><span style="width: 40px;"><label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand"><input onchange="addChild(this,{{ $i }});" type="checkbox" name="merge[]" value="{{ $lot->id }}">&nbsp;<span></span></label></span>
                                </td>
                                <td>
                                    <a href="{{ route('LotDetailsShow',['id'=>$lot->id]) }}">
                                        {{ $lot->id }}
                                    @foreach ($lot->Childs as $child )
                                     | {{ $child->id }}
                                    <?php $quantity+=$child->quantity_inlot ?>
                                    <input class="child{{ $i }}"  type="checkbox" style="visibility: hidden" name="merge[]" value="{{ $child->id }}">
                                    @endforeach
                                    </a>
                                </td>
                                <td>{{ $quantity }} </td>
                                <td>
                                    <input type="submit" class="btn btn-outline-success m-btn m-btn--custom m-btn--outline-2x" name="do" value="UnMerge">
                                </td>
                            </tr>

                                @elseif(empty($lot->Parent))
                                <td class="m-datatable__cell m-datatable__cell--check"><span style="width: 40px;"><label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand"><input type="checkbox" name="merge[]" value="{{ $lot->id }}">&nbsp;<span></span></label></span></td>
                                <td><a href="{{ route('LotDetailsShow',['id'=>$lot->id]) }}">{{ $lot->id }}</a></td>
                                <td>{{ $lot->quantity_inlot }}</td>


                                <td >
                                        {{-- <a href="{{ route('LotUpdateForm',['id'=>$lot->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                            <i class="la la-edit"></i>
                                        </a> --}}
                                    </td>
                                </tr>

                                @endif

                             <?php $i++;?>
                        @empty
                             <tr>
                                 <td colspan="5">Not Available</td>
                             </tr>
                        @endforelse
                    </tbody>
                </table>
                <input type="submit" class="btn btn-outline-success m-btn m-btn--custom m-btn--outline-2x" value="Merge"/>

                </form>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    <script src="js/dataTables.min.js"></script>
    <script>
            $(document).ready( function () {
                $('#m_table_2').DataTable();
            } );
    </script>

     {{--  <script src="{{ asset('/assets/demo/default/custom/crud/datatables/basic/scrollable.js') }}"></script>  --}}
<script>

        function addChild(checkbo,index) {
            console.log(checkbo);
            if ($(checkbo).is(':checked')) {
                //  alert(index);
                $('.child'+index).each(function(){
                    $(this).prop('checked', true);

                });
                }else{
                    $('.child'+index).each(function(){
                        $(this).prop('checked', false);

                    });
                }
          };

</script>
@endsection
