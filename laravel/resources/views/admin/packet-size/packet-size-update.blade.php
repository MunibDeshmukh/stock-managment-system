@extends('layouts.master')

@section('content')
<div class="m-content">
    <div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->

<!--end::Portlet-->

<!--begin::Portlet-->
<div class="m-portlet">
<div class="m-portlet__head">
<div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
        <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
            Update Packet Size
        </h3>
    </div>
</div>
</div>
<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('Packet_sizeUpdate') }}" method="POST" enctype="multipart/form-data">
<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">Packet_size Name:</label>
        <div class="col-lg-3">
            <input type="text" name="packet_size" class="form-control m-input" placeholder="Enter Packet_size Name" value="{{ $packet_size->packet_size }}">
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions--solid">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-10">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{{ $packet_size->id }}">
                <input type="submit" class="btn btn-success"/>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </div>
</div>
</form>
<!--end::Form-->
</div>

</div>
</div>
</div>
@endsection
