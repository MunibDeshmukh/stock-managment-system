@extends('layouts.master')

@section('content')
<div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Packet Sizes Data
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('Packet_sizeShowForm') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>Add Packet_size</span>
                                </span>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table id="m_table_2">
                    <thead>
                        <tr>
                                <th>Packet_size Name</th>
                                <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($packet_sizes as $packet_size)
                        <tr role="row" class="odd">
                                <td >{{ $packet_size->packet_size }}</td>
                                <td nowrap="">

                                    <a href="{{ route('Packet_sizeUpdateForm',['id'=>$packet_size->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                        <i class="la la-edit"></i>
                                    </a>
                                </td>
                             </tr>
                        @empty
                             <tr>
                                 <td colspan="5">Not Available</td>
                             </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    <script src="js/dataTables.min.js"></script>
    <script>
            $(document).ready( function () {
                $('#m_table_2').DataTable({
                    paging:true
                });
            } );
    </script>

     {{--  <script src="{{ asset('/assets/demo/default/custom/crud/datatables/basic/scrollable.js') }}"></script>  --}}

@endsection
