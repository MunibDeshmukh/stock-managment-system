@extends('layouts.master')

@section('content')
<div class="m-content">
    <div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->

<!--end::Portlet-->

<!--begin::Portlet-->
<div class="m-portlet">
<div class="m-portlet__head">
<div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
        <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
            Update Product {{ $product->prod_name }}
        </h3>
    </div>
</div>
</div>
<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('ProductUpdate') }}" method="POST" enctype="multipart/form-data">
<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">Product Name:</label>
        <div class="col-lg-3">
            <input type="text" name="prod_name" class="form-control m-input" placeholder="Enter Product Name" value="{{ $product->prod_name }}">
        </div>
        <label class="col-lg-2 col-form-label">Product Code:</label>
        <div class="col-lg-3">
            <input type="tel" name="prod_code" class="form-control m-input" placeholder="Enter Product Code" value="{{ $product->prod_code }}">
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">Unit:</label>
        <div class="col-lg-3">
            <div class="m-input-icon m-input-icon--right">
                <select class="form-control m-input" id="exampleSelect1" name="unit_id">
                        <option value="{{ $product->Unit->id }}">{{ $product->Unit->unit }}</option>
                        @forelse ($units as $unit)
                        @if ($unit->id!=$product->Unit->id)
                        <option value="{{ $unit->id }}">{{ $unit->unit }}</option>
                        @endif
                        @empty

                        @endforelse
                    </select>
                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions--solid">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-10">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{{ $product->id }}">
                <input type="submit" class="btn btn-success"/>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </div>
</div>
</form>
<!--end::Form-->
</div>

</div>
</div>
</div>
@endsection
