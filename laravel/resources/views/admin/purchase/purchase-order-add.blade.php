@extends('layouts.master')

@section('content')
<div class="m-content">
    <div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->

<!--end::Portlet-->

<!--begin::Portlet-->
<div class="m-portlet">
<div class="m-portlet__head">
<div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
        <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
            Add Purchase Order
        </h3>
    </div>
    @if (Session::has('message'))
        {!! Session('message') !!}
    @endif
</div>
</div>
<!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('Purchase_ordersAdd') }}" method="POST" enctype="multipart/form-data">
<div class="m-portlet__body">
    <div class="form-group m-form__group row">
            <label class="col-lg-1 col-form-label">Lot No:</label>
            <div class="col-lg-3">
                <input type="number" name="lot_id" class="form-control m-input" readonly placeholder="" value="{{ $lot_id }}">
            </div>
            <label class="col-lg-1 col-form-label">Vendor:</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--right">
                    <select class="form-control m-input" id="exampleSelect1" name="vendor_id" required>
                            <option>Select Vendor</option>
                            @forelse ($vendors as $vendor)
                            <option value="{{ $vendor->id }}">{{ $vendor->companyname }}</option>
                            @empty

                            @endforelse
                        </select>
                </div>
            </div>
        <label class="col-lg-1 col-form-label">Date:</label>
        <div class="col-lg-3">
                <div class="input-group date">
						<input type="date" class="form-control m-input"  placeholder="Select Date" name="date" id="m_datepicker_2">

                 </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-lg-1 col-form-label">BL. No:</label>
        <div class="col-lg-3">
            <input type="text" name="bl_number" class="form-control m-input" placeholder="Enter BL. No">
        </div>
        <label class="col-lg-1 col-form-label">GD. No:</label>
        <div class="col-lg-3">
            <input type="text" name="gd_number" class="form-control m-input" placeholder="Enter GD. No">
        </div>

        <label class="col-lg-1 col-form-label">Invoice Weight:</label>
        <div class="col-lg-3">
            <input type="text" name="invoice_weight" class="form-control m-input" placeholder="Enter Invoice Weight">
        </div>
    </div>
    <div class="form-group m-form__group row">

        <label class="col-lg-1 col-form-label">Arrival Weight:</label>
        <div class="col-lg-3">
            <input type="text" name="arrival_weight" class="form-control m-input" placeholder="Enter Arrival Weight">
        </div>
        <label class="col-lg-1 col-form-label">Sale Weight:</label>
        <div class="col-lg-3">
            <input type="text" name="sale_weight" class="form-control m-input" placeholder="Enter Sale Weight">
        </div>
        <label class="col-lg-1 col-form-label">Products:</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--right">
                    <select class="form-control m-input" id="exampleSelect1" name="productName" required onchange="getProduct(this.value);">
                            <option>Select Product</option>
                            @forelse ($products as $product)
                            <option value="{{ $product->id }}">{{ $product->prod_name }}</option>
                            @empty

                            @endforelse
                        </select>
                </div>
            </div>
            <input type="hidden" id="total_price" name="total_amount" value=0>
            <input type="hidden" id="total_quantity" name="total_quantity" value=0>


    </div>
    <div class="form-group m-form__group row">
            <div class="m-section col-lg-12">
                    <div class="m-section__content">
                        <table id="myproducts" class="table m-table m-table--head-bg-brand">
                              <thead>
                                <tr>
                                      <th>#</th>
                                      <th>Product Name</th>
                                      <th>Product Code</th>
                                      <th>Cost Price</th>
                                      <th>Quantity</th>
                                      <th>No. Bags</th>
                                      <th>Packet Size</th>
                                      <th>SubTotal</th>
                                      <th>Remove</th>
                                </tr>
                              </thead>
                              <tbody id="tbr">

                              </tbody>
                        </table>
                    </div>
            </div>
    </div>

</div>
<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions m-form__actions--solid">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-10">
                {!! csrf_field() !!}
                <input type="submit" class="btn btn-success"/>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div >
        </div>
    </div>
</div>
</form>
<!--end::Form-->
</div>

</div>
</div>
</div>
    <script>

        var i=1;
        var tp=0;
        var tq=0;
        var str='<td><select class="form-control m-input" id="exampleSelect1" name="packet_size_id[]" required>';
                  @forelse ($packet_sizes as $packet_size)
                str+='<option value="{{ $packet_size->id }}">{{ $packet_size->packet_size }}</option>';
                @empty

                @endforelse
                str+='</select></td>';
           console.log(str);
        function getProduct(id){
            //alert(id);
            var prodid=$('#'+id).length;
            if(prodid==0){
                $.ajax({
                    url:'products-get/'+id,
                    type:'GET',
                    success:function(data){
                        $('#tbr').append('<tr id='+data.id+'>'
                            +'<td>'+parseInt(window.i)+'</td>'
                            +'<td>'+data.prod_name+'</td>'
                            +'<td>'+data.prod_code+'</td>'
                            +'<td id="cost-price"><input onkeyup="calc($(this))" class="form-control m-input" type="number" name="cost_price[]" min=1 required id="example-number-input"></td>'
                            +'<td><input id="qt'+data.id+'" onkeyup="calc($(this))" class="form-control m-input" type="number" name="quantity[]" min=1  value="1" id="example-number-input"></td>'
                            +'<td><input id="bags'+data.id+'" onkeyup="calc($(this))" class="form-control m-input" type="number" name="bags[]" min=1  value="1" id="example-number-input"></td>'
                            +str+'<td id="tolat-price"><input readonly class="form-control m-input" type="number" name="subtotal[]" min=1  value="'+data.cost_price+'" id="example-number-input"></td>'
                            +'<td><div class="m-demo-icon__preview" onClick="document.getElementById(\'myproducts\').deleteRow('+window.i+');"><i class="flaticon-cancel"  ></i></div></td>'
                        +'</tr><input type="hidden"  name="product_id[]" value='+data.id+'>')

                        window.i++;
                    }
                });
            }
            else{
                document.getElementById('qt'+id).value++;
                $("tr").each(function(){
                    var tpCount= $(this).find('td').eq(5).find("input").val();
                    var tqCount= $(this).find('td').eq(4).find("input").val();
                    if(tpCount!=null){

                       tp+=parseInt(tpCount);
                       tq+=parseInt(tqCount);
                       document.getElementById("total_price").value=tp;
                       document.getElementById("total_quantity").value=tq;
                       console.log("totalPrice......: "+ document.getElementById("total_price").value);
                       console.log("totalQuanity......: "+document.getElementById("total_quantity").value);
                    }


                });
            }

        }

        function calc(elm){

            var tr = elm.closest('tr');
            var p = tr.find('td').eq(3).find('input').val();
            var q = tr.find('td').eq(4).find('input').val();
            var st=tr.find('td').eq(7).find('input').val(parseInt(p)*parseInt(q));
            $("tr").each(function(){
                var tpCount= $(this).find('td').eq(5).find("input").val();
                var tqCount= $(this).find('td').eq(4).find("input").val();
                if(tpCount!=null){

                   tp+=parseInt(tpCount);
                   tq+=parseInt(tqCount);
                   document.getElementById("total_price").value=tp;
                   document.getElementById("total_quantity").value=tq;
              	 console.log("totalPrice......: "+ document.getElementById("total_price").value);
                   console.log("totalQuanity......: "+document.getElementById("total_quantity").value);
                }


            });
        }
    </script>
@endsection
