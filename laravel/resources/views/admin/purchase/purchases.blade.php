@extends('layouts.master')

@section('content')
<div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Purchases Table
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('Purchase_ordersShowForm') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>Add Purchase</span>
                                </span>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table id="m_table_2">
                    <thead>
                        <tr>
                                <th>Vendor Name</th>
                                <th>BL. No</th>
                                <th>GD. No</th>
                                <th>Invoice Weight</th>
                                <th>Arrival Weight</th>
                                <th>Sale Weight</th>
                                <th>Lot Id</th>
                                <th>Total Balance</th>
                                <th>Date</th>
                                <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($purchases as $purchase)
                        <tr role="row" class="odd">
                                <td >{{ $purchase->Vendor->companyname }}</td>
                                <td>{{ $purchase->bl_number }}</td>
                                <td>{{ $purchase->gd_number }}</td>
                                <td>{{ $purchase->invoice_weight }}</td>
                                <td>{{ $purchase->arrival_weight }}</td>
                                <td>{{ $purchase->sale_weight }}</td>
                                <td>{{ $purchase->Lot->id }}</td>
                                <td>{{ $purchase->total_amount }}</td>
                                <td>{{ $purchase->date }}</td>
                                <td nowrap="">

                                    {{-- <a href="{{ route('PurchaseUpdateForm',['id'=>$purchase->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                        <i class="la la-edit"></i>
                                    </a> --}}
                                </td>
                             </tr>
                        @empty
                             <tr>
                                 <td colspan="5">Not Available</td>
                             </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    <script src="js/dataTables.min.js"></script>
    <script>
            $(document).ready( function () {
                $('#m_table_2').DataTable();
            } );
    </script>

     {{--  <script src="{{ asset('/assets/demo/default/custom/crud/datatables/basic/scrollable.js') }}"></script>  --}}

@endsection
