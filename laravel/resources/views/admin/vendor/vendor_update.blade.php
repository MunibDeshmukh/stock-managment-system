@extends('layouts.master')

@section('content')
<div class="m-content">
        <div class="row">
    <div class="col-lg-12">
    <!--begin::Portlet-->
    <div class="m-portlet">
    <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
            <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                Update Vendor {{ $vendor->companyname }}
            </h3>
        </div>
    </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('VendorUpdate') }}" method="POST">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">Company Name:</label>
            <div class="col-lg-3">
                <input type="text" name="companyname" class="form-control m-input" placeholder="Enter company name" value="{{ $vendor->companyname }}">
            </div>
            <label class="col-lg-2 col-form-label">Contact Person:</label>
            <div class="col-lg-3">
                <input type="tel" name="contact_person" class="form-control m-input" placeholder="Enter contact person" value="{{ $vendor->contact_person }}">
            </div>
        </div>
        <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Contact Number:</label>
                <div class="col-lg-3">
                    <input type="tel" name="contact_number" class="form-control m-input" placeholder="Enter contact number" value="{{ $vendor->contact_number }}" >
                </div>
            <label class="col-lg-2 col-form-label">Address:</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--right">
                    <input type="text" name="address" class="form-control m-input" placeholder="Enter your address" value="{{ $vendor->address }}">
                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
                </div>
            </div>

        </div>
        <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Email:</label>
                <div class="col-lg-3">
                    <div class="m-input-icon m-input-icon--right">
                        <input type="email" name="email" class="form-control m-input" placeholder="Enter company email" value="{{ $vendor->email }}">
                        <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-bookmark-o"></i></span></span>
                    </div>
                </div>
                <label class="col-lg-2 col-form-label">Opening Balance:</label>
                <div class="col-lg-3">
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number" name="opening_balance" class="form-control m-input" placeholder="Enter company email" value="{{ $vendor->opening_balance }}">
                        <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-bookmark-o"></i></span></span>
                    </div>
                </div>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions--solid">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-10">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $vendor->id }}">
                    <input type="submit" class="btn btn-success"/>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    </form>
    <!--end::Form-->
    </div>

    </div>
    </div>
    </div>
@endsection
