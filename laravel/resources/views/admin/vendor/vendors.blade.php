@extends('layouts.master')

@section('content')
<div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Vendors Data
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>New Order</span>
                                </span>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table id="m_table_2">
                    <thead>
                        <tr>
                                <th>Vendor Name</th>
                                <th>Contact Person</th>
                                <th>Contact Number</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Opening Balance</th>
                                <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($vendors as $vendor)
                        <tr role="row" class="odd">
                                <td >{{ $vendor->companyname }}</td>
                                <td>{{ $vendor->contact_person }}</td>
                                <td>{{ $vendor->contact_number }}</td>
                                <td>{{ $vendor->email }}</td>
                                <td>{{ $vendor->address }}</td>
                                <td>{{ $vendor->opening_balance }}</td>
                                <td nowrap="">

                                    <a href="{{ route('VendorUpdateForm',['id'=>$vendor->id]) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                        <i class="la la-edit"></i>
                                    </a>
                                </td>
                             </tr>
                        @empty
                             <tr>
                                 <td colspan="5">Not Available</td>
                             </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    <script src="js/dataTables.min.js"></script>
    <script>
            $(document).ready( function () {
                $('#m_table_2').DataTable();
            } );
    </script>

     {{--  <script src="{{ asset('/assets/demo/default/custom/crud/datatables/basic/scrollable.js') }}"></script>  --}}

@endsection
