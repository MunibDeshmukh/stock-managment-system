<?php

use App\Vendor;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login','LoginController@Login' )->name('LoginShow');
Route::post('signIn','LoginController@SignIn')->name('signIn');
Route::post('signUp','LoginController@CreateUser')->name('createUser');

Route::get('/home',function(){
    return view('layouts.master');
})->name('Home');

//-------------------------------------------------VENDOR-----------------------------------------------//
Route::get('/vendor-add-form','VendorController@VendorShowForm')->name('VendorShowForm');
Route::post('vendor-add','VendorController@VendorAdd')->name('VendorAdd');
Route::get('/vendor-update/{id}','VendorController@VendorUpdateForm')->name('VendorUpdateForm');
Route::post('/vendor-update','VendorController@VendorUpdate')->name('VendorUpdate');
Route::get('/vendors','VendorController@VendorShow')->name('VendorShow');
Route::get('/Vendor-remove/{id}','VendorController@VendorRemove')->name('VendorRemove');

//-------------------------------------------------CUSTOMER---------------------------------------//
Route::get('/customer-add-form','CustomerController@CustomerShowForm')->name('CustomerShowForm');
Route::post('customer-add','CustomerController@CustomerAdd')->name('CustomerAdd');
Route::get('/customer-update/{id}','CustomerController@CustomerUpdateForm')->name('CustomerUpdateForm');
Route::post('/customer-update','CustomerController@CustomerUpdate')->name('CustomerUpdate');
Route::get('/customers','CustomerController@CustomerShow')->name('CustomerShow');
Route::get('/customer-remove/{id}','CustomerController@CustomerRemove')->name('CustomerRemove');

//-------------------------------------PRODUCT----------------------------------------------------------//
Route::get('/product-add-form','ProductController@ProductShowForm')->name('ProductShowForm');
Route::post('product-add','ProductController@ProductAdd')->name('ProductAdd');
Route::get('/product-update/{id}','ProductController@ProductUpdateForm')->name('ProductUpdateForm');
Route::post('/product-update','ProductController@ProductUpdate')->name('ProductUpdate');
Route::get('/products','ProductController@ProductShow')->name('ProductShow');
Route::get('/products-get/{id}','ProductController@ProductGet')->name('ProductGet');
Route::get('/product-remove/{id}','ProductController@ProductRemove')->name('ProductRemove');

//-----------------------------------UNIT---------------------------------------------------------------//

Route::get('/unit-add-form','UnitController@UnitShowForm')->name('UnitShowForm');
Route::post('unit-add','UnitController@UnitAdd')->name('UnitAdd');
Route::get('/unit-update/{id}','UnitController@UnitUpdateForm')->name('UnitUpdateForm');
Route::post('/unit-update','UnitController@UnitUpdate')->name('UnitUpdate');
Route::get('/units','UnitController@UnitShow')->name('UnitShow');
Route::get('/unit-remove/{id}','UnitController@UnitRemove')->name('UnitRemove');

//----------------------------------PACKET-SIZES------------------------------------------------------//

Route::get('/packet_size-add-form','PacketsizeController@Packet_sizeShowForm')->name('Packet_sizeShowForm');
Route::post('packet_size-add','PacketsizeController@Packet_sizeAdd')->name('Packet_sizeAdd');
Route::get('/packet_size-update/{id}','PacketsizeController@Packet_sizeUpdateForm')->name('Packet_sizeUpdateForm');
Route::post('/packet_size-update','PacketsizeController@Packet_sizeUpdate')->name('Packet_sizeUpdate');
Route::get('/packet_sizes','PacketsizeController@Packet_sizeShow')->name('Packet_sizeShow');
Route::get('/packet_size-remove/{id}','PacketsizeController@Packet_sizeRemove')->name('Packet_sizeRemove');

//-------------------------------STOCK LOT------------------------------------------------//

Route::get('/stock','StockController@StockShow')->name('StockShow');
Route::get('/lots','StockController@LotShow')->name('LotShow');
Route::post('/LotsMerge','StockController@LotMerge')->name('LotMerge');
Route::get('/lot-products/{id}','StockController@LotDetailsShow')->name('LotDetailsShow');


//---------------------------------------PURCHASES-------------------------------------//
Route::get('/purchases-add','PurchasesController@Purchase_ordersShowForm')->name('Purchase_ordersShowForm');
Route::post('/purchases-add','PurchasesController@Purchase_ordersAdd')->name('Purchase_ordersAdd');
Route::get('/purchases','PurchasesController@PurchaseShow')->name('Purchases');


