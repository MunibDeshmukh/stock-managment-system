-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2019 at 01:56 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstname`, `lastname`, `email`, `contact_number`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Munib', 'Deshmukh', 'admin@gmail.com', '12345', 'unkno', '2019-01-01 03:46:23', '2019-01-01 03:48:42', NULL),
(2, 'Munib', 'Deshmukh', 'admin@gmail.com', '12345', 'unknow', '2019-01-01 03:46:28', '2019-01-01 03:46:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lots`
--

CREATE TABLE `lots` (
  `id` int(10) UNSIGNED NOT NULL,
  `quantity_inlot` int(11) NOT NULL,
  `lot_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lots`
--

INSERT INTO `lots` (`id`, `quantity_inlot`, `lot_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(19, 300, NULL, '2018-12-31 03:43:29', '2018-12-31 03:43:29', NULL),
(20, 1001, NULL, '2018-12-31 04:02:58', '2019-01-03 00:44:09', NULL),
(21, 100, NULL, '2018-12-31 04:06:22', '2019-01-03 00:45:03', NULL),
(22, 10, NULL, '2018-12-31 04:07:47', '2019-01-03 00:45:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lot_products`
--

CREATE TABLE `lot_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `lot_id` int(10) UNSIGNED NOT NULL,
  `packet_size_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity_inlot` int(11) NOT NULL,
  `bags` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cost_price` float UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lot_products`
--

INSERT INTO `lot_products` (`id`, `product_id`, `lot_id`, `packet_size_id`, `quantity_inlot`, `bags`, `cost_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, 10, 19, 1, 100, 1, 22, '2018-12-31 03:43:29', '2018-12-31 03:43:29', NULL),
(10, 10, 19, 1, 100, 1, 22, '2018-12-31 03:43:29', '2018-12-31 03:43:29', NULL),
(11, 10, 20, 1, 1, 1, 1000, '2018-12-31 04:02:58', '2018-12-31 04:02:58', NULL),
(12, 10, 20, 1, 1, 1, 1000, '2018-12-31 04:02:58', '2018-12-31 04:02:58', NULL),
(13, 12, 21, 1, 100, 1200, 2000, '2018-12-31 04:06:22', '2018-12-31 04:06:22', NULL),
(14, 11, 22, 1, 10, 1, 100, '2018-12-31 04:07:47', '2018-12-31 04:07:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_12_24_121835_create_users_table', 1),
(2, '2018_12_24_124038_create_units_table', 2),
(4, '2018_12_24_125823_create_customers_table', 2),
(5, '2018_12_24_124907_create_products_table', 3),
(6, '2018_12_27_070406_create_packet_sizes_table', 4),
(8, '2018_12_27_081143_create_stock_table', 5),
(9, '2018_12_27_110001_create_purchase_order_table', 6),
(11, '2018_12_28_071111_create__lots_table', 7),
(12, '2019_01_01_073349_create_sales_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `packet_sizes`
--

CREATE TABLE `packet_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `packet_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packet_sizes`
--

INSERT INTO `packet_sizes` (`id`, `packet_size`, `created_at`, `updated_at`) VALUES
(1, '10x2', '2018-12-27 02:58:05', '2018-12-27 02:58:46');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `prod_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prod_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `prod_name`, `prod_code`, `unit_id`, `created_at`, `updated_at`) VALUES
(10, 'tapaal', 'tp-01', 1, '2018-12-28 06:23:13', '2018-12-28 06:23:13'),
(11, 'tapaal1', 'tp-03', 2, '2018-12-28 06:23:33', '2018-12-28 06:23:33'),
(12, 'tapaal2', 'tp-02', 2, '2018-12-28 06:23:42', '2018-12-28 06:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

CREATE TABLE `purchase_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `total_amount` double(8,2) NOT NULL DEFAULT '0.00',
  `lot_id` int(10) UNSIGNED DEFAULT NULL,
  `date` date NOT NULL,
  `invoice_weight` int(11) NOT NULL DEFAULT '0',
  `arrival_weight` int(11) NOT NULL DEFAULT '0',
  `sale_weight` int(11) NOT NULL DEFAULT '0',
  `bl_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gd_number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_orders`
--

INSERT INTO `purchase_orders` (`id`, `vendor_id`, `total_amount`, `lot_id`, `date`, `invoice_weight`, `arrival_weight`, `sale_weight`, `bl_number`, `gd_number`, `created_at`, `updated_at`) VALUES
(4, 1, 0.00, 19, '2018-12-26', 12, 12, 12, 'bl-01', 'gd-02', '2018-12-31 03:43:29', '2018-12-31 03:43:29'),
(5, 1, 0.00, 20, '2018-12-25', 12, 12, 12, 'Bl-02', 'GD-02', '2018-12-31 04:02:58', '2018-12-31 04:02:58'),
(6, 1, 0.00, 22, '2018-12-19', 12, 12, 12, 'asa', 'sad', '2018-12-31 04:07:48', '2018-12-31 04:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `pur_odr_details`
--

CREATE TABLE `pur_odr_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `pur_odr_id` int(10) UNSIGNED NOT NULL,
  `prod_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_amount` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', NULL, NULL),
(2, 'USER', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `total_amount` double(8,2) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `prod_id` int(10) UNSIGNED NOT NULL,
  `quantity_instock` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity_sold` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity_purchased` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `prod_id`, `quantity_instock`, `quantity_sold`, `quantity_purchased`, `created_at`, `updated_at`) VALUES
(5, 10, 237, 0, 237, '2018-12-28 06:23:13', '2019-01-01 02:43:48'),
(6, 11, 204, 0, 204, '2018-12-28 06:23:33', '2019-01-01 02:43:48'),
(7, 12, 1223, 0, 1223, '2018-12-28 06:23:42', '2019-01-01 02:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'KG', NULL, '2018-12-26 09:19:49'),
(2, 'litre', NULL, NULL),
(3, 'oz', '2018-12-26 09:19:30', '2018-12-26 09:19:30'),
(4, 'lb', '2018-12-26 09:20:02', '2018-12-26 09:20:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `username`, `password`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'Munib', 'Deshmukh', 'admin@gmail.com', 'Admin', '$2y$10$WR/pn6cRiWx.K5UwVgwAnOcDXE5jPO5BWHmD8JCNpWRQQOej5hC8W', 1, '2018-12-26 02:12:03', '2018-12-26 02:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `companyname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` double(8,2) NOT NULL DEFAULT '0.00',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `companyname`, `contact_person`, `contact_number`, `email`, `opening_balance`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'starsoft', '03221222', '303033031', 'admin@gmail.c', 1016.00, 'unknow', '2018-12-26 03:11:29', '2018-12-26 05:53:14', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lots`
--
ALTER TABLE `lots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lots_lot_id_foreign` (`lot_id`);

--
-- Indexes for table `lot_products`
--
ALTER TABLE `lot_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lot_products_lot_id_foreign` (`lot_id`),
  ADD KEY `foreign_lot_products_packet_size_id` (`packet_size_id`),
  ADD KEY `lot_products_product_id_foreign_idx` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packet_sizes`
--
ALTER TABLE `packet_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_orders_vendor_id_foreign` (`vendor_id`),
  ADD KEY `purchase_orders_lot_id_foreign` (`lot_id`);

--
-- Indexes for table `pur_odr_details`
--
ALTER TABLE `pur_odr_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pur_odr_details_pur_odr_id_foreign` (`pur_odr_id`),
  ADD KEY `pur_odr_details_prod_id_foreign` (`prod_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stocks_prod_id_foreign` (`prod_id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lots`
--
ALTER TABLE `lots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `lot_products`
--
ALTER TABLE `lot_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `packet_sizes`
--
ALTER TABLE `packet_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pur_odr_details`
--
ALTER TABLE `pur_odr_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lots`
--
ALTER TABLE `lots`
  ADD CONSTRAINT `lots_lot_id_foreign` FOREIGN KEY (`lot_id`) REFERENCES `lots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lot_products`
--
ALTER TABLE `lot_products`
  ADD CONSTRAINT `foreign_lot_products_packet_size_id` FOREIGN KEY (`packet_size_id`) REFERENCES `packet_sizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lot_products_lot_id_foreign` FOREIGN KEY (`lot_id`) REFERENCES `lots` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lot_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD CONSTRAINT `purchase_orders_lot_id_foreign` FOREIGN KEY (`lot_id`) REFERENCES `lots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_orders_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pur_odr_details`
--
ALTER TABLE `pur_odr_details`
  ADD CONSTRAINT `pur_odr_details_prod_id_foreign` FOREIGN KEY (`prod_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pur_odr_details_pur_odr_id_foreign` FOREIGN KEY (`pur_odr_id`) REFERENCES `purchase_orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_prod_id_foreign` FOREIGN KEY (`prod_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
